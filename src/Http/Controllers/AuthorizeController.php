<?php

namespace BoneyBone\AuthServer\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use BoneyBone\AuthServer\AuthServer;
use BoneyBone\AuthServer\Services\Store\SessionStore;

class AuthorizeController extends Controller
{
    /**
     * The AuthServer instance.
     *
     * @var  AuthServer $authServer
     */
    protected $authServer;

    /**
     * The Store instance.
     *
     * @var  Store $store
     */
    protected $store;

    /**
     * Build the class.
     *
     * @param  AuthServer $authServer
     * @return void
     */
    public function __construct(AuthServer $authServer, SessionStore $store) {
        $this->authServer = $authServer;
        $this->store      = $store;
    }

    /**
     * Redirect to the authorization server.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function redirect(Request $request)
    {
        $authorizationUrl = $this->authServer->getAuthorizationUrl();

        $returnUrl = $this->store->get('return_to');

        // set current url as redirect back.
        if(!$returnUrl) {

            $fallbackReturn = url()->previous();

            if(strpos($fallbackReturn, config('auth-server.redirect_uri'))){
                $fallbackReturn  = $this->authServer->getRedirectTo();
            }

            $this->store->set('return_to', $fallbackReturn ?? $this->authServer->getRedirectTo());
        }

        return redirect($authorizationUrl);
    }

    /**
     * Handle the authorization callback.
     *
     * @param  Request $request
     * @return Response
     */
    public function callback(Request $request) {

        if( ! $request->has('code') ) return abort(404);

        $state = $this->authServer->getState();

        $token = $this->authServer->exchangeCodeIntoToken($request->code);

        $this->authServer->setAccessToken($token);

        $this->authServer->login();

        $this->authServer->resetState();

        $redirectUrl = $this->store->get('return_to');

        return view('auth-server::redirector', compact('redirectUrl'));
    }
}

<?php

namespace BoneyBone\AuthServer\Http\Middleware;

use BoneyBone\AuthServer\Contracts\Store;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AuthServerAuthenticationMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  Illuminate\Http\Request $request
     * @param  Closure                 $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $intendedUrl = encrypt($request->url());

        if(!$request->user()) {

            app(Store::class)->set('return_to', $request->url());

            return redirect()->away(route('login', ['next' => $intendedUrl]));
        }

        return $next($request);

    }
}

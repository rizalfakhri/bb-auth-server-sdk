<?php

namespace BoneyBone\AuthServer\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use BoneyBone\AuthServer\AuthServer;
use Illuminate\Auth\AuthenticationException;
use BoneyBone\AuthServer\BoneyBoneUser;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class AuthServerMiddleware {

    /**
     * The AuthServer instance.
     *
     * @var  AuthServer $authServer
     */
    protected $authServer;

    /**
     * The GuzzleHttp Client.
     *
     * @var  Client $client
     */
    protected $client;

    /**
     * The cache expired time in minutes.
     *
     * @var  $cacheExpiration
     */
    protected $cacheExpiration = 60 * 2; // 2 hours

    /**
     * Build the middleware
     *
     * @param  AuthServe $authServer
     * @return void
     */
    public function __construct(AuthServer $authServer) {
        $this->authServer = $authServer;

        $this->client  = new Client([
            'base_uri' => $authServer->getAuthServerEndpoint()
        ]);
    }

    /**
     * Handle an incoming request.
     *
     * @param  Illuminate\Http\Request $request
     * @param  Closure                 $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        if( ! is_null($request->bearerToken()) ) {
            $bearerToken = $request->bearerToken();
            $cacheKey     = md5("cached_bb_sdk_userinfo");
            $cachedInfo   = Cache::get($cacheKey);

            if( is_null($cachedInfo) ) {

                $userInfoRes = $this->client->get("/api/me?with=client_access,client_roles", [
                    'headers' => [
                        'Authorization' => sprintf("Bearer %s", $bearerToken)
                    ]
                ]);

                $rawInfo = json_decode($userInfoRes->getBody()->getContents(), true);

                // cache the user info to minimize latency.
                Cache::put($cacheKey, $rawInfo, now()->addMinutes($this->cacheExpiration));
            }
            else
            {
                $rawInfo = $cachedInfo;
            }


            $request->setUserResolver(function() use($rawInfo) {
                return new BoneyBoneUser($rawInfo);
            });

            Auth::login(new BoneyBoneUser($rawInfo));

            return $next($request);

        }

        throw new AuthenticationException;
    }
}

<?php

namespace BoneyBone\AuthServer\Providers;

use BoneyBone\AuthServer\AuthServer;
use Illuminate\Support\ServiceProvider;
use BoneyBone\AuthServer\Services\Store\SessionStore;
use BoneyBone\AuthServer\Contracts\Store as StoreContract;
use BoneyBone\AuthServer\BoneyBoneUserProvider;
use Illuminate\Support\Facades\Auth;

class AuthServerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            StoreContract::class,
            SessionStore::class
        );

        $this->app->bind('bb-auth-server', AuthServer::class);
        $this->app->singleton(AuthServer::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Register the routes.
        $this->loadRoutesFrom(__DIR__ . '/../routes/auth-server-routes.php');

        // Register the views
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'auth-server');

        // Publish the required thins.
        $this->publishes([
            __DIR__ . '/../../config/auth-server.php' => config_path('auth-server.php')
        ], 'auth-server');

        Auth::provider('boneybone-auth-server', function($app, array $config = []) {
            return $app->make(BoneyBoneUserProvider::class);
        });
    }
}

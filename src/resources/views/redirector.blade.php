<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Redirecting...</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv = "refresh" content = "2; {{($redirectUrl) ? $redirectUrl : app('bb-auth-server')->getRedirectTo()}}" />
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

<div class="relative flex min-h-screen flex-col justify-center overflow-hidden bg-gray-50 py-6 sm:py-12">
  <div class="absolute inset-0 bg-[#6039B6]"></div>
  <div class="relative bg-white px-6 pb-8 pt-10 shadow-xl ring-1 ring-gray-900/5 sm:mx-auto sm:max-w-lg sm:rounded-lg sm:px-10">
    <div class="mx-auto max-w-md">
      <div class="divide-y divide-gray-300/50">
        <div class="space-y-6 py-8 text-base leading-7 text-gray-600">
          <h3 class="mb-12 text-center text-xl font-bold text-gray-900">Authorization Successful</h3>
          <div class="mb-12 flex items-center justify-center space-x-0">
            <div class="flex w-3/12 items-center justify-center">
              <div class="flex h-20 w-20 items-center justify-center rounded-full bg-[#6039B6] p-2">
                <img src="https://auth.ixtelecom.net/img/new-logos/ppl.png" class="h-auto" />
              </div>
            </div>

            <div class="relative w-6/12 items-center justify-center">
              <div class="absolute -mt-5 flex h-10 w-full items-center justify-center">
                <div class="border-3 flex h-10 w-10 items-center justify-center rounded-full border-white bg-green-500">
                  <div class="w-6 h-6 text-white">
                    <svg fill="currentColor" stroke="currentColor"  stroke-linejoin="round" stroke-miterlimit="2" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m2.25 12.321 7.27 6.491c.143.127.321.19.499.19.206 0 .41-.084.559-.249l11.23-12.501c.129-.143.192-.321.192-.5 0-.419-.338-.75-.749-.75-.206 0-.411.084-.559.249l-10.731 11.945-6.711-5.994c-.144-.127-.322-.19-.5-.19-.417 0-.75.336-.75.749 0 .206.084.412.25.56" fill-rule="nonzero" /></svg>
                  </div>
                </div>
              </div>
              <div class="w-full border-t-2 border-dashed border-gray-200"></div>
            </div>
            <div class="flex w-3/12 items-center justify-center">
              <div class="flex h-20 w-20 items-center justify-center rounded-full bg-[#6039B6] p-2">
                  @if(env('AUTH_SERVER_APP_ICON'))
                      <img src="{{env('AUTH_SERVER_APP_ICON')}}" class="h-auto" />
                  @else
                      <img src="https://erp-people.s3.ap-southeast-1.amazonaws.com/default-app-icon.png" alt="" class="h-auto"/>
                  @endif

              </div>
            </div>
          </div>
          <p class="mt-12">Please wait while we redirect you back to the application and continue your work...</p>
        </div>
        <div class="pt-8 text-base font-semibold leading-7">
          <p class="text-gray-900">This page is not redirecting within 5 seconds?</p>
          <p>
            <a href="{{($redirectUrl) ? $redirectUrl : app('bb-auth-server')->getRedirectTo()}}" class="text-sky-500 hover:text-sky-600">Click Here &rarr;</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>

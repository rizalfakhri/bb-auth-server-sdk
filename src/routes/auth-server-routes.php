<?php

Route::group(['prefix' => 'auth', 'middleware' => 'web'], function() {

    Route::group(['middleware' => 'auth'], function() {
        Route::get('deauthorize', 'BoneyBone\AuthServer\Http\Controllers\DeauthorizeController@deauthorize');
    });

    Route::group(['middleware' => 'guest'], function() {
        Route::get('authorize', 'BoneyBone\AuthServer\Http\Controllers\AuthorizeController@redirect')->name('login');
        Route::get('callback', 'BoneyBone\AuthServer\Http\Controllers\AuthorizeController@callback');
    });


});

Route::group(['prefix' => 'auth', 'middleware' => 'api'], function() {
    Route::post('token', 'BoneyBone\AuthServer\Http\Controllers\AccessTokenController@exchangeAuthorizationCode');
    Route::post('token/refresh', 'BoneyBone\AuthServer\Http\Controllers\AccessTokenController@refreshAccessToken');
});
